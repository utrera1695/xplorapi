'use strict'

var path = require('path')
var fs = require('fs')
var mongoosePaginate = require('mongoose-pagination')
var Cuestionario = require('../models/Cuestionario')
var Pregunta = require('../models/Pregunta')
var Opciones = require('../models/Opciones')

function getOpciones(req,res){
	var preguntaId = req.params.pregunta;

	Opciones.findOne({pregunta : preguntaId},(err,opc)=>{
		if(err){
			res.status(500).send({message:'Error obtener las opciones'})
		}else{
			if(!opc){
				res.status(404).send({message:'No se pudo obtener las opciones '+preguntaId})
			}else{
				res.status(200).send({opc})
			}
		}
	})
}

function saveOpciones(req,res){
	var opciones = new Opciones()

	var params = req.body
	opciones.opcion1 = params.opcion1
	opciones.opcion2 = params.opcion2
	opciones.opcion3 = params.opcion3
	opciones.opcion4 = params.opcion4
	opciones.pregunta = params.pregunta
	

	opciones.save((err,opcionStored)=>{
		if(err){
			res.status(500).send({message:'Error al guardar cuestionario'})
		}else{
			if(!opcionStored){
				res.status(404).send({message:'No se pudo guardar el cuestionario'})
			}else{
				res.status(200).send({opciones: opcionStored})
			}
		}
	})
}

function update(req,res){
	var opcionId = req.params.opcion
	var update = req.body

	Opciones.findByIdAndUpdate(opcionId, update,(err,opcionUpdate)=>{
		if(err){
			res.status(500).send({message: 'Error al intentar modificar la opcion'})
		}else{
			if(!opcionUpdate){
				res.status(404).send({message:'No se pudo modificar la opcion'})
			}else{
				res.status(200).send({opcion: opcionUpdate})
			}
		}
	})
}



module.exports ={
	getOpciones,
	saveOpciones,
	update,
}

