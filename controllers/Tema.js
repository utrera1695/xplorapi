'use strict'

var path = require('path')
var fs = require('fs')
var Tema = require('../models/Tema')

function saveTema(req,res){
	var tema = new Tema()
	var params = req.body
	tema.fraseP= params.fraseP
	tema.nombre= params.nombre
	tema.fraseF= params.fraseF
	tema.archivo= '';
	tema.save((err, temaStored) =>{
		if(err){
			res.status(500).send({message: 'Error al guardar al tema'})
		}else{
			if(!temaStored){
				res.status(404).send({message: 'No se ha registrado el tema'})
			}else{
				res.status(200).send({tema: temaStored})
			}
		}
	})
}
function deleteTema(req, res){
	var temaId = req.params.tema;

	Tema.findByIdAndRemove(temaId,(err, temaDelet)=>{
		if(err){
			res.status(500).send({message: 'Error al intentar eliminar el cuestionario'})
		}else{
			if(!temaDelet){
				res.status(404).send({message: 'No se pudo eliminar el cuestionario'})
			}else{
				var path ='./public/images/'+temaDelet.archivo
				fs.unlinkSync(path);
				res.status(200).send({tema: temaDelet})
			}
		}
	})
}
function uploadImage(req,res){
   var temaId= req.params.tema;
  if(req.files){
  	var file_path =JSON.stringify(req.files.image.path);
    var file_split = file_path.split("\\\\");
  	var file_name = JSON.stringify(file_split[2]);
  	var ext_split = file_name.split("\.");
  	var file_ext = ext_split[1];
  	var file_extR = file_ext.substr(0,3)
  	var file_nameR = JSON.parse((file_name.replace('\\"','')));
	//res.status(200).send({file_nameR})

  	if((file_extR == 'png')||(file_extR == 'PNG')){
  		Tema.findByIdAndUpdate(temaId,{archivo:file_nameR},(err,temaUpdate)=>{
  			if(!temaUpdate){
  				res.status(404).send({message: 'No se pudo subir la imagen'})
  			}else{
  				res.status(300).send({tema: temaUpdate})
  			}
  		})
  	}else{
  		res.status(200).send({message: 'Solo se admiten archivos .png'})
  	}

  	
  }else{
  	res.status(400).send({message: 'No se ha subido ninguna imagen'})
	
  }
}
function getImage(req,res){
	var imageFile = req.params.imageFile
	var path_file = './public/images/'+imageFile;
	fs.exists(path_file,function(exists){
		if(exists){
			res.sendFile(path.resolve(path_file))
		}else{
			res.status(400).send({message: 'No existe la imagen'})
		}
	})
}
function getTema(req,res){
	var temaId = req.params.tema;

	Tema.findById(temaId, (err,tema)=>{
		if(err){
			res.status(500).send({message:'Error buscar el tema'})
		}else{
			if(!tema){
				res.status(404).send({message:'No se encontro el tema'})
			}else{
				res.status(200).send({tema: tema})
			}
		}
	})
}

function listTemas(req,res){
	if(req.params.page){
		var page = req.params.page
		var items = 8
	}else{
		var items = 10000
		var page = 1
	}

	Tema.find().sort('_id').paginate(page, items,function(err, tema, total){
		if(err){
			res.status(500).send({message: 'Error al listar los cuestionarios'})
		}else{
			if(!tema){
				res.status(404).send({message: 'No se pudo listar los cuestionarios'})
			}else{
				res.status(200).send({
					temas: tema,total
				})
			}
		}
	})
}
function update(req,res){
	var temaId = req.params.tema
	var update = req.body
	Tema.findByIdAndUpdate(temaId,update,(err,temaUpdate)=>{
  			if(!temaUpdate){
  				res.status(404).send({message: 'No se pudo editar el tema'})
  			}else{
  				res.status(300).send({tema: temaUpdate})
  			}
  		})
}
module.exports={
	uploadImage,
	getImage,
	saveTema,
	getTema,
	listTemas,
	deleteTema,
	update
}
