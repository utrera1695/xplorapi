'use strict'

var bcrypt = require('bcrypt-nodejs')
var Kiosko = require ('../models/Kiosko')
var jwt = require('../services/jwt')


function getKiosko(req,res){
	var kioskoId = req.params.kiosko;

	Kiosko.findById(kioskoId, (err,k)=>{
		if(err){
			res.status(500).send({message:'Error buscar el kiosko'})
		}else{
			if(!k){
				res.status(404).send({message:'No se encontro el kiosko'})
			}else{
				res.status(200).send({kiosko: k})
			}
		}
	})
}

function SaveKiosko(req, res){
	var kiosko = new Kiosko()
	var params = req.body

	kiosko.nombre=params.nombre
	kiosko.id = params.id
	kiosko.cuestionario = params.cuestionario
	kiosko.time = 0
	kiosko.estado= false
	kiosko.tema = params.tema
	if(kiosko.nombre != null){
		kiosko.save((err, kioskoStored) =>{
			if(err){
				res.status(500).send({message: 'Error al guardar kiosko'})
			}else{
				if(!kioskoStored){
					res.status(404).send({message: 'No se ha guardado kiosko'})
				}else{
					
					res.status(200).send({kiosko: kioskoStored})
				}
			}
		})
	}else{
		res.status(200).send({message: 'Debe especificar un nombre de kiosko'});
	}
}

function LogKiosko(req,res){
	var params =req.body
	var kid = params.id
	Kiosko.findOne({id: kid }, (err, kiosko) =>{
		if(err){
			res.status(500).send({message: 'Error al buscar al usuario'})
		}else{
			if(!kiosko){
				res.status(404).send({message: 'EL usuario no existe'})
			}else{
				res.status(200).send({token: jwt.createToken(kiosko),kiosko})
			}
				
		}
	})
}

function listKiosko(req,res){
	if(req.params.page){
		var page = req.params.page
		var items = 10000
	}else{
		var page = 1
		var items = 8
	}

	Kiosko.find().sort({$natural:-1}).paginate(page, items,function(err, kiosko, total){
		if(err){
			res.status(500).send({message: 'Error al listar los kioskos'})
		}else{
			if(!kiosko){
				res.status(404).send({message: 'No se pudo listar los kiosko'})
			}else{
				res.status(200).send({kioskos: kiosko})
			}
		}
	})
}

function deleteKiosko(req, res){
	var kioskoId = req.params.kiosko;

	Kiosko.findByIdAndRemove(kioskoId,(err, kioskoDelet)=>{
		if(err){
			res.status(500).send({message: 'Error al intentar eliminar el cuestionario'})
		}else{
			if(!kioskoDelet){
				res.status(404).send({message: 'No se pudo eliminar el cuestionario'})
			}else{
				res.status(200).send({kiosko: kioskoDelet})
			}
		}
	})
}

function kioskoUpdate(req,res){
	var kioskoId = req.params.kiosko
	var update = req.body

	Kiosko.findByIdAndUpdate(kioskoId,update, (err, kioskoUpdate)=>{
		if(err){
			res.status(500).send({message: 'Error al intentar actualizar al usuario'})
		}else{
			if(!kioskoUpdate){
				res.status(404).send({message: 'No se pudo actualizar el cuestionario'})
			}else{
				res.status(200).send({kiosko: kioskoUpdate})
			}
		}
	})
}
function kioskoUpdate2(req,res){
	var kioskoId = req.params.kiosko
	var update = req.body

	Kiosko.findByIdAndUpdate(kioskoId,{time: update.time,estado:update.estado}, (err, kioskoUpdate)=>{
		if(err){
			res.status(500).send({message: 'Error al intentar actualizar al usuario'})
		}else{
			if(!kioskoUpdate){
				res.status(404).send({message: 'No se pudo actualizar el cuestionario'})
			}else{
				res.status(200).send({kiosko: kioskoUpdate})
			}
		}
	})
}

module.exports = {
	SaveKiosko,
	LogKiosko,
	listKiosko,
	deleteKiosko,
	kioskoUpdate,
	getKiosko,
	kioskoUpdate2
}