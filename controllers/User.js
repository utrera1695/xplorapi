'Use Strict'

var User= require('../models/User')


function saveUser(req,res){
	var user = new User()
	var params = req.body

	user.nombre= params.nombre
	user.apellido= params.apellido
	user.edad= params.edad
	user.sexo= params.sexo
	user.pais= params.pais
	user.ciudad= params.ciudad
	user.telefono= params.telefono
	user.correo= params.correo
	user.cuestionario =params.cuestionario
	user.save((err,userStored)=>{
		if(err){
			res.status(500).send({message: 'Error al intentar guardar los datos del usuario'})
		}else{
			if(!userStored){
				res.status(404).send({message: 'no se pudo guardar al usuario'})
			}else{
				res.status(200).send({user: userStored})
			}
		}

	})
}
function listUsers(req,res){
	var cuestionarioId = req.params.cuestionario
	User.find({cuestionario: cuestionarioId}).sort('_id').paginate(1,1000,function(err, users, total){
		if(err){
			res.status(500).send({message: 'Error al listar los cuestionarios'})
		}else{
			if(!users){
				res.status(404).send({message: 'No se pudo listar los cuestionarios'})
			}else{
				return res.status(200).send({
					user: users,total
				})
			}
		}
	})
}
function listUsers2(req,res){
	User.find().sort('_id').paginate(1,1000,function(err, users, total){
		if(err){
			res.status(500).send({message: 'Error al listar los cuestionarios'})
		}else{
			if(!users){
				res.status(404).send({message: 'No se pudo listar los cuestionarios'})
			}else{
				return res.status(200).send({
					user: users,total
				})
			}
		}
	})
}

module.exports ={
	saveUser,
	listUsers,
	listUsers2
}