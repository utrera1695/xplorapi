'use strict'

var path = require('path')
var fs = require('fs')
var mongoosePaginate = require('mongoose-pagination')
var Cuestionario = require('../models/Cuestionario')
var Pregunta = require('../models/Pregunta')
var Opciones = require('../models/Opciones')
var ReporteC = require('../models/ReporteC')

function saveCuestionario(req,res){
	var cuestionario = new Cuestionario()
	var params = req.body
	cuestionario.nombreC = params.nombreC,
	cuestionario.estado = 'false'

	cuestionario.save((err,cuestionarioStored)=>{
		if(err){
			res.status(500).send({message: 'Error al crear el cuestionario'})
		}else{
			if(!cuestionarioStored){
				res.status(404).send({message: 'No se ha creado el cuestionario'})
			}else{
				var reporteC= new ReporteC()
				 reporteC.cuestionario = cuestionarioStored._id
				 reporteC.cuestionarioN = cuestionarioStored.nombreC
				
				reporteC.save((err,reportecStored)=>{
					if(err){
						res.status(500).send({message: 'Error al crear el reporte'})
					}else{
						if(!reportecStored){
							res.status(404).send({message: 'No se ha creado Archivo de reporte'})
						}else{
							res.status(200).send({cuestionario: cuestionarioStored})
						}
					}
				})
			}
		}
	})
}

function getCuestionario(req,res){
	var cuestionarioId = req.params.cuestionarioId;

	Cuestionario.findById(cuestionarioId, (err,cuestion)=>{
		if(err){
			res.status(500).send({message:'Error al buscar cuestionario'})
		}else{
			if(!cuestion){
				res.status(404).send({message:'No se pudo encontrar el cuestionario'})
			}else{
				res.status(200).send({cuestion})
			}
		}

	})
}

function listCuestionario(req,res){
	if(req.params.page){
		var page = req.params.page
		var items = 12
	}else{
		var items = 10000
		var page = 1
	}

	Cuestionario.find().sort('_id').paginate(page, items,function(err, cuestion, total){
		if(err){
			res.status(500).send({message: 'Error al listar los cuestionarios'})
		}else{
			if(!cuestion){
				res.status(404).send({message: 'No se pudo listar los cuestionarios'})
			}else{
				res.status(200).send({
					cuestion: cuestion,total
				})
			}
		}
	})
}

function updateCuestionario(req,res){
	var cuestionarioId = req.params.cuestionario
	var update = req.body

	Cuestionario.findByIdAndUpdate(cuestionarioId,update, (err, cuestionarioUpdate)=>{
		if(err){
			res.status(500).send({message: 'Error al intentar actualizar al usuario'})
		}else{
			if(!cuestionarioUpdate){
				res.status(404).send({message: 'No se pudo actualizar el cuestionario'})
			}else{
				ReporteC.find({cuestionario: cuestionarioId}).update({cuestionarioN:update.nombreC },(err,reporteCUpdate)=>{
					if(err){

					}else{
						if(!reporteCUpdate){

						}
					}
				})
				res.status(200).send({cuestionario: cuestionarioUpdate})
			}
		}
	})
}

function deleteCuestionario(req, res){
	var cuestionarioId = req.params.cuestionario;

	Cuestionario.findByIdAndRemove(cuestionarioId,(err, cuestionarioDelet)=>{
		if(err){
			res.status(500).send({message: 'Error al intentar eliminar el cuestionario'})
		}else{
			if(!cuestionarioDelet){
				res.status(404).send({message: 'No se pudo eliminar el cuestionario'})
			}else{
				//res.status(200).send({cuestionario: cuestionarioDelet})

				Pregunta.find({cuestionario: cuestionarioDelet._id}).remove((err, preguntaDelet)=>{
						if(err){
							res.status(500).send({message: 'Error al intentar eliminar las preguntas '})
						}else{
							if(!preguntaDelet){
								res.status(404).send({message: 'No se pudo eliminar las preguntas'})
							}else{
								
								Opciones.find({pregunta: preguntaDelet._id}).remove((err,opcionesDelet)=>{
									if(err){
										res.status(500).send({message: 'Error al intentar eliminar las opciones de la pregunta'})
									}else{
										if(!opcionesDelet){
											res.status(404).send({message: 'No se pudo eliminar la las opciones de la pregunta'})
										}else{
											res.status(200).send({cuestionario: cuestionarioDelet, pregunta: preguntaDelet,opciones: opcionesDelet})
										}
									}
								})
							}
						}
					})
			}
		}
	})
}

module.exports ={
	saveCuestionario,
	getCuestionario,
	listCuestionario,
	deleteCuestionario,
	updateCuestionario
}