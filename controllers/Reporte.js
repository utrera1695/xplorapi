var path = require('path')
var fs = require('fs')
var mongoosePaginate = require('mongoose-pagination')
var Cuestionario = require('../models/Cuestionario')
var Reporte = require('../models/reporte')

function saveReporte(req,res){
	var reporte = new Reporte();
	var params = req.body;

	reporte.rpregunta1 = params.rpregunta1;
	reporte.rpregunta2 = params.rpregunta2;
	reporte.rpregunta3 = params.rpregunta3;
	reporte.rpregunta4 = params.rpregunta4;
	reporte.rpregunta5 = params.rpregunta5;
	reporte.cuestionario = params.cuestionario;
	reporte.user = params.user;
	reporte.save((err,reporteStored)=>{
		if(err){
			res.status(500).send({message:"Error al guardar las respuestas del cuestionario"})
		}else{
			if(!reporteStored){
				res.status(404).send({message:"No se pudo guardar las respuestas del cuestionario"})
			}else{
				res.status(200).send({reporte: reporteStored})
			}
		}

	})
}

function listReportes(req,res){
	var cuestionarioId = req.params.cuestionario
	
	 Reporte.find({cuestionario: cuestionarioId}).paginate(1,1000,function(err,reporte,total){
		if(err){
			res.status(500).send({message: 'Error al intentar listar los resportes'})
		}else{
			if(!reporte){
				res.status(404).send({message: 'No hay reportes que mostrar'})
			}else{
				res.status(200).send({reportes: reporte,total})
			}
		}
	})

}
function listAllReportes(req,res){

	Reporte.find().paginate(1,10000,function(err, reporte, total){
		if(err){
			res.status(500).send({message: 'Error al listar los cuestionarios'})
		}else{
			if(!reporte){
				res.status(404).send({message: 'No se pudo listar los cuestionarios'})
			}else{
				return res.status(200).send({reporte,total})
			}
		}
	})
}

module.exports={
	saveReporte,
	listReportes,
	listAllReportes

}