'use strict'

var path = require('path')
var fs = require('fs')
var mongoosePaginate = require('mongoose-pagination')
var Cuestionario = require('../models/Cuestionario')
var Pregunta = require('../models/Pregunta')
var Opciones = require('../models/Opciones')

function getPregunta(req,res){
	var preguntaId = req.params.id;

	pregunta.findById(preguntaId, (err,pregunta)=>{
		if(err){
			res.status(500).send({message:'Error al guardar cuestionario'})
		}else{
			if(!pregunta){
				res.status(404).send({message:'No se pudo guardar el cuestionario'})
			}else{
				res.status(200).send({pregunta})
			}
		}
	})
}

function savePregunta(req,res){
	var pregunta = new Pregunta()

	var params = req.body
	pregunta.nombre =params.nombre
	pregunta.tipo=params.tipo
	pregunta.cuestionario = params.cuestionario

	pregunta.save((err,preguntaStored)=>{
		if(err){
			res.status(500).send({message:'Error al guardar Pregunta'})
		}else{
			if(!preguntaStored){
				res.status(404).send({message:'No se pudo guardar la pregunta'})
			}else{
				res.status(200).send({pregunta: preguntaStored,id:preguntaStored._id})
			}
		}
	})
}

function listPreguntas(req,res){
	var cuestionarioId = req.params.cuestionario
	
	if(req.params.page){
		var items= 2
		var page = req.params.page
	}else{
		var items=50
		var page = 1
	}
	if(cuestionarioId){
		Pregunta.find({cuestionario: cuestionarioId}).sort('_id').paginate(page, items,function(err, pregunta, total){
			if(err){
				res.status(500).send({message: 'Error al listar los cuestionarios'})
			}else{
				if(!pregunta){
					res.status(404).send({message: 'No se pudo listar los cuestionarios'})
				}else{
					return res.status(200).send({
						pregunta: pregunta
					})
				}
			}
		})
	}else{
		Pregunta.find().sort('_id').paginate(page, items,function(err, pregunta, total){
			if(err){
				res.status(500).send({message: 'Error al listar los cuestionarios'})
			}else{
				if(!pregunta){
					res.status(404).send({message: 'No se pudo listar los cuestionarios'})
				}else{
					return res.status(200).send({
						pregunta: pregunta,total
					})
				}
			}
		})
	}
}

function updatePregunta(req,res){
	var preguntaId = req.params.id
	var update = req.body

	Pregunta.findByIdAndUpdate(preguntaId, update,(err,preguntaUpdate)=>{
		if(err){
			res.status(500).send({message: 'Error al intentar modificar la pregunta'})
		}else{
			if(!preguntaUpdate){
				res.status(404).send({message:'No se pudo modificar la pregunta'})
			}else{
				res.status(200).send({pregunta: preguntaUpdate})
			}
		}
	})
}

function deletePregunta(req,res){
	var preguntaId = req.params.pregunta

	Pregunta.findByIdAndRemove(preguntaId,(err, preguntaDelet)=>{
		if(err){
			res.status(500).send({message: 'Error al intentar eliminar la pregunta'})
		}else{
			if(!preguntaDelet){
				res.status(404).send({message: 'No se pudo eliminar la pregunta'})
			}else{
				//SE ELIMINAN LAS OPCIONES DE PREGUNTA QUE SE ENCUENTRAN DENTRO
				Opciones.find({pregunta: preguntaDelet._id}).remove((err,opcionesDelet)=>{
					if(err){
						res.status(500).send({message: 'Error al intentar eliminar las opciones de la pregunta'})
					}else{
						if(!opcionesDelet){
							res.status(404).send({message: 'No se pudo eliminar la las opciones de la pregunta'})
						}else{
							res.status(200).send({pregunta: preguntaDelet,opciones: opcionesDelet})
						}
					}
				})
			}
		}
	})

}

module.exports ={
	getPregunta,
	savePregunta,
	listPreguntas,
	updatePregunta,
	deletePregunta
}

