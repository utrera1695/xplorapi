'use strict'

var path = require('path')
var fs = require('fs')
var mongoosePaginate = require('mongoose-pagination')
var Cuestionario = require('../models/Cuestionario')
var Pregunta = require('../models/Pregunta')
var Opciones = require('../models/Opciones')
var ReporteC = require('../models/ReporteC')

function listReportes(req,res){
	if(req.params.page){
		var page = req.params.page
		var items = 5
	}else{
		var items = 1000000
		var page = 1
	}

	ReporteC.find().sort({$natural:-1}).paginate(page, items,function(err, reportec, total){
		if(err){
			res.status(500).send({message: 'Error al listar los cuestionarios'})
		}else{
			if(!reportec){
				res.status(404).send({message: 'No se pudo listar los cuestionarios'})
			}else{
				res.status(200).send({reportc: reportec,total})
			}
		}
	})
}

function Delete(req,res){
	var reportesId = req.params.reportes;

	ReporteC.findByIdAndRemove(reportesId,(err, reportesDelet)=>{
		if(err){
			res.status(500).send({message: 'Error al intentar eliminar el reporte'})
		}else{
			if(!reportesDelet){
				res.status(404).send({message: 'No se pudo eliminar el reporte'})
			}else{
				res.status(200).send({reportesc: reportesDelet})
			}
		}
	})
}

function getReporte(req,res){
	var reporteId = req.params.reporte;
	Reporte.findById(reporteId, (err,reporte)=>{
		if(err){
			res.status(500).send({message:'Error buscar el reporte'})
		}else{
			if(!reporte){
				res.status(404).send({message:'No se encontro el reporte'})
			}else{
				res.status(200).send({reporte: reporte})
			}
		}
	})
}

module.exports ={
	listReportes,
	Delete,
	getReporte
}