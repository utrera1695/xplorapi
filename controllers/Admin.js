'use strict'

var bcrypt = require('bcrypt-nodejs')
var Admin = require ('../models/admin')
var jwt = require('../services/jwt')


function guardarAdmin(req, res){
	var admin = new Admin()
	var params = req.body

	//console.log({params})
	admin.username=params.username;
	admin.adminrol='A';
	admin.name= params.name;
	admin.email= params.email;
	admin.pass= params.pass;
	if(params.password){
		bcrypt.hash(params.password,null,null, function(err,hash){
			admin.password = hash;
			if(admin.username != null){
				admin.save((err, userStored) =>{
					if(err){
						res.status(500).send({message: 'Error al guardar al administrador'})
					}else{
						if(!userStored){
							res.status(404).send({message: 'No se ha registrado al usuario'})
						}else{
							res.status(200).send({admin: userStored})
						}
					}
				})
			}else{
				res.status(200).send({message: 'Debe especificar un nombre de usuario'});
			}

		});
	}else{
		res.status(200).send({message: 'Debe introducir una contraseña'});
	}
}
function getAdmin(req,res){
	var adminname = req.params.admin;

	Admin.findOne({username: adminname.toLowerCase()}, (err,admin)=>{
		if(err){
			res.status(500).send({message:'Error buscar el administrador'})
		}else{
			if(!admin){
				res.status(200).send({message:'No se encontro el administrado'})
			}else{
				res.status(200).send({admin: admin})
			}
		}
	})
}
function getAdminEmail(req,res){
	var adminemail = req.params.email;

	Admin.findOne({email: adminemail.toLowerCase()},(err,admin)=>{
		if(err){
			res.status(500).send({message:'Error buscar el administrador'})
		}else{
			if(!admin){
				res.status(200).send({message:'No se encontro el administrado'})
			}else{
				res.status(200).send({admin: admin})
			}
		}
	})
}

function loginAdmin(req,res){
	var params =req.body
	var username = params.username
	var password = params.password
	Admin.findOne({username: username.toLowerCase()}, (err, user) =>{
		if(err){
			res.status(500).send({message: 'Error al buscar al usuario'})
		}else{
			if(!user){
				res.status(200).send({message: 'EL usuario no existe'})
			}else{
				bcrypt.compare(password, user.password, function(err, check){
					if(check){
						if(params.gethash){
							//SE DEVUELVE TOKEN DE JWT
							
							res.status(200).send({
								token: jwt.createToken(user)
							})
						}else{
							res.status(200).send({user})
						}
					}else{
						res.status(200).send({message: 'No se ha podido acceder'})
					}
				})
			}
		}
	})

}

function listAdmins(req,res){
	if(req.params.page){
		var page = req.params.page
	}else{
		var page = 1
	}

	Admin.find().sort('_id').paginate(page, 1000,function(err, admin, total){
		if(err){
			res.status(500).send({message: 'Error al listar los admins'})
		}else{
			if(!admin){
				res.status(404).send({message: 'No se pudo listar los cuestionarios'})
			}else{
				res.status(200).send({
					admins: admin,total
				})
			}
		}
	})
}

function adminDelete(req, res){
	var adminId = req.params.admin;

	Admin.findByIdAndRemove(adminId,(err, adminDelet)=>{
		if(err){
			res.status(500).send({message: 'Error al intentar eliminar el administrador'})
		}else{
			if(!adminDelet){
				res.status(404).send({message: 'No se pudo eliminar el administrador'})
			}else{
				res.status(200).send({admin: adminDelet})
			}
		}
	})
}

function adminUpdate(req,res){
	var adminId = req.params.admin
	var update = req.body
	bcrypt.hash(update.password,null,null, function(err,hash){
		update.password=hash
		Admin.findByIdAndUpdate(adminId, update, (err, adminUpdate)=>{
			if(err){
				res.status(500).send({message: 'Error al intentar actualizar al usuario'})
			}else{
				if(!adminUpdate){
					res.status(404).send({message: 'No se pudo actualizar el cuestionario'})
				}else{
					res.status(200).send({adminU: adminUpdate})
				}
			}
		})
	})

}

module.exports ={
	loginAdmin,
	guardarAdmin,
	listAdmins,
	adminDelete,
	adminUpdate,
	getAdmin,
	getAdminEmail
}