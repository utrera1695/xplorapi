'use strict'
var nodemailer = require('nodemailer') 
var Admin = require ('../models/admin')

// email sender function  
function sendEmail (req, res){  
// Definimos el transporter      
	var admin = req.body;
	
	var transporter = nodemailer.createTransport({          
		host:'smtpout.secureserver.net',
		port:'465',
		tls: { rejectUnauthorized: false },
		secure: true,          
		auth: {              
			user: 'recuperacion@getxplor.com',              
			pass: 'R3cup3r4ci0n$'          
		}      
	})  // Definimos el email  
	var mailOptions = {      
		from: 'recuperacion@getxplor.com',      
		to: admin.email+',xplorecovery@jamtechcorp.com',      
		subject: 'Xplor - Recuperación de contraseña',      
		text:'Feliz día '+admin.name+'.\n\n'+
			 'Hemos registrado una solicitud de cambio de contraseña a tu cuenta de administrador.\n'+
			 'Estos son tus datos de inicio de sesión: \n'+
			 '	Nombre de usuario: '+admin.username+'\n'+
			 '	Contraseña:        '+admin.pass+'\n\n\n'+
			 'Si desconoces esta solicitud, ponte en contacto con tu administrador de cuenta, Saludos.' 
	}  // Enviamos el email  
	transporter.sendMail(mailOptions, function(error, info){
		if (error){
			console.log(error)
			res.send(500, error.message)
        }else{
  	        console.log("Email sent")
  	        res.status(200).send({info})
        }  
	})  
}

module.exports={
	sendEmail
}