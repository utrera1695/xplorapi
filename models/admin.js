'use strict'

var mongoose = require('mongoose')
var Schema = mongoose.Schema

var AdminSchema = Schema({
	username: String,
	password: String,
	adminrol: String,
	name: String,
	email: String,
	pass: String
})

module.exports = mongoose.model('Admin',AdminSchema)