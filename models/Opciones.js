'use strict'

var mongoose = require('mongoose')
var Schema = mongoose.Schema

var OpcionesSchema = Schema({
	opcion1: String,
	opcion2: String,
	opcion3: String,
	opcion4: String,
	pregunta: {type: Schema.ObjectId, ref:'Pregunta'}
})

module.exports = mongoose.model('Opciones',OpcionesSchema)