'use strict'

var mongoose = require('mongoose')
var Schema = mongoose.Schema

var KioskoSchema = Schema({
	nombre: String,
	id: String,
	time: Number,
	estado: Boolean,
	cuestionario: {type: Schema.ObjectId, ref:'Cuestionario'},
	tema: {type: Schema.ObjectId, ref:'Tema'}
})

module.exports = mongoose.model('Kiosko',KioskoSchema)