'use strict'

var mongoose = require('mongoose')
var Schema = mongoose.Schema

var UserSchema = Schema({
	nombre: String,
	apellido: String,
	edad: String,
	sexo: String,
	pais: String,
	ciudad: String,
	telefono: String,
	correo: String,
	cuestionario: {type: Schema.ObjectId, ref:'Cuestionario'},
	
})

module.exports = mongoose.model('User',UserSchema)