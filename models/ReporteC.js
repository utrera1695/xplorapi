'use strict'

var mongoose = require('mongoose')
var Schema = mongoose.Schema

var ReporteCSchema = Schema({
	cuestionario: {type: Schema.ObjectId, ref:'Cuestionario'},
	cuestionarioN: String,
})

module.exports = mongoose.model('ReporteC',ReporteCSchema)