'use strict'

var mongoose = require('mongoose')
var Schema = mongoose.Schema

var PreguntaSchema = Schema({
	nombre: String,
	tipo: String,
	posicion: Number,
	cuestionario: {type: Schema.ObjectId, ref:'Cuestionario'}
})

module.exports = mongoose.model('Pregunta',PreguntaSchema)