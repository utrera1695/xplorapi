'use strict'

var mongoose = require('mongoose')
var Schema = mongoose.Schema

var ReporteSchema = Schema({
	cuestionario: {type: Schema.ObjectId, ref:'Cuestionario'},
	user: {type: Schema.ObjectId, ref: 'User'},
	rpregunta1: String,
	rpregunta2: String,
	rpregunta3: String,
	rpregunta4: String,
	rpregunta5: String,
})

module.exports = mongoose.model('Reporte',ReporteSchema)