'use strict'

var mongoose = require('mongoose')
var Schema = mongoose.Schema

var TemaSchema = Schema({
	archivo: String,
	nombre: String,
	fraseP: String,
	fraseF: String
})

module.exports = mongoose.model('Tema',TemaSchema)