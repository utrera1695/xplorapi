'use strict'

var mongoose = require('mongoose')
var Schema = mongoose.Schema

var CuestionarioSchema = Schema({
	nombreC: String,
	estado: Boolean
})

module.exports = mongoose.model('Cuestionario',CuestionarioSchema)