'use strict'

var express = require('express')
var bodyParser = require('body-parser')

var app = express()
var cors = require("cors");
//CARGAR RUTAS

var admin_routes = require('./routes/Admin')
var cuestionario_routes = require('./routes/cuestionario')
var pregunta_routes = require('./routes/Preguntas')
var reportes_routes = require('./routes/Reportes')
var opciones_routes = require('./routes/Opciones')
var kiosko_routes = require('./routes/Kiosko')
var reporteC_routes = require('./routes/ReporteC')
var user_routes = require('./routes/User')
var tema_routes = require('./routes/Tema')
var mail_routes = require('./routes/mail')
app.use(bodyParser.urlencoded({extended:true,limit:'2mb'}))
app.use(bodyParser.json())
app.use(cors());

//configurar cabeceras http
app.use((req, res, next)=>{
	res.header('Access-Control-Allow-Origin','*');
	res.header('Access-Control-Allow-Headers','Authorization,X-API-KEY,Origin,X-Rquested-Widht,Content-Type,Accept,Acces-Control-Allow-Request-Method');
	res.header('Access-Control-Allow-Methods','GET, POST, ÖPTIONS, PUT, DELETE');
	res.header('Allow','GET, POST, ÖPTIONS, PUT, DELETE');
	next()
})

//rutas base
app.use('/api',admin_routes)
app.use('/api',cuestionario_routes)
app.use('/api',pregunta_routes)
app.use('/api',reportes_routes)
app.use('/api',opciones_routes)
app.use('/api',kiosko_routes)
app.use('/api',reporteC_routes)
app.use('/api',user_routes)
app.use('/api',tema_routes)
app.use('/api',mail_routes)

module.exports = app;