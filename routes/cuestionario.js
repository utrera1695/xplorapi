
'use strict'

var express = require('express')
var CuestionController = require('../controllers/cuestionario')
var md_auth = require('../midleware/authenticated')
var api = express.Router()

api.post('/cuestionario',/*md_auth.ensureAuth,*/ CuestionController.saveCuestionario)
api.get('/cuestionario/:cuestionarioId',CuestionController.getCuestionario)
api.get('/listeCuestionario',/*md_auth.ensureAuth,*/CuestionController.listCuestionario)
api.get('/listeCuestionario/:page',/*md_auth.ensureAuth,*/CuestionController.listCuestionario)
api.delete('/deletCuestionario/:cuestionario',/*md_auth.ensureAuth,*/ CuestionController.deleteCuestionario)	
api.put('/cuestionario/:cuestionario',/*md_auth.ensureAuth,*/ CuestionController.updateCuestionario)

module.exports = api