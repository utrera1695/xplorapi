'use strict'

var express = require('express')
var ReporteController = require('../controllers/Reporte')
var md_auth = require('../midleware/authenticated')
var api = express.Router()

api.post('/reporte', ReporteController.saveReporte)
api.get('/listReportes/:cuestionario',ReporteController.listReportes)
api.get('/listAll',ReporteController.listAllReportes)

module.exports = api