'use strict'

var express = require('express')
var md_auth = require('../midleware/authenticated')
var TemaController = require('../controllers/Tema')
var api = express.Router()
var multipart = require('connect-multiparty');
var multipartMiddleware = multipart({uploadDir:'./public/images'});
var multer  = require('multer');

api.post('/upload/:tema', multipartMiddleware,TemaController.uploadImage);
api.get('/getImage/:imageFile',TemaController.getImage)
api.get('/gettema/:tema',TemaController.getTema)
api.get('/listtemas/:page',TemaController.listTemas)
api.get('/listtemas',TemaController.listTemas)
api.post('/savetema',TemaController.saveTema)
api.put('/updatetema/:tema',TemaController.update)
api.delete('/deleteTema/:tema',TemaController.deleteTema)
/*api.post('/upload/:tema', upload.single('file'), function(req, res, next)
{
    res.json(req.file)
})*/
module.exports = api