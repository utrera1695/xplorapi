var express = require('express')
var ReportecController = require('../controllers/ReporteC')
var md_auth = require('../midleware/authenticated')
var api = express.Router()

api.get('/reportec/:reporte',/*md_auth.ensureAuth,*/ReportecController.getReporte)
api.get('/listReportec',/*md_auth.ensureAuth,*/ReportecController.listReportes)
api.get('/listReportec/:page',/*md_auth.ensureAuth,*/ReportecController.listReportes)
api.delete('/delete/:reportes',/*md_auth.ensureAuth,*/ReportecController.Delete)


module.exports = api