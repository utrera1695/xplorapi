'use strict'

var express = require('express')
var MailController = require('../controllers/mail')
var api = express.Router()

api.post('/sendmail',MailController.sendEmail)
module.exports = api