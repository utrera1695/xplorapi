'use strict'

var express = require('express')
var PreguntaController = require('../controllers/Preguntas')
var md_auth = require('../midleware/authenticated')
var api = express.Router()


api.post('/pregunta',/*md_auth.ensureAuth, */PreguntaController.savePregunta)
api.get('/pregunta/:pregunta', md_auth.ensureAuth,PreguntaController.getPregunta)
api.get('/preguntas/:cuestionario',PreguntaController.listPreguntas)
api.get('/preguntas',PreguntaController.listPreguntas)
api.put('/preguntaupdate/:id',/*md_auth.ensureAuth,*/PreguntaController.updatePregunta)
api.get('/preguntas/:cuestionario/:page',PreguntaController.listPreguntas)
api.delete('/pregunta/:pregunta'/*,md_auth.ensureAuth,*/,PreguntaController.deletePregunta)

module.exports = api