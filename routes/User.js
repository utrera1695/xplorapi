'use strict'

var express = require('express')
var UserController = require('../controllers/User')
var api = express.Router()

api.post('/save',UserController.saveUser)
api.get('/listUsers/:cuestionario',UserController.listUsers)
api.get('/listUsers',UserController.listUsers2)

module.exports=api