'use strict'

var express = require('express')
var KioskoController = require('../controllers/Kiosko')
var md_auth = require('../midleware/authenticated')
var api = express.Router()


api.post('/kiosko',KioskoController.SaveKiosko)
api.post('/loginkiosko',KioskoController.LogKiosko)
api.get('/listekiosko',KioskoController.listKiosko)
api.get('/listekiosko/:page',KioskoController.listKiosko)
api.get('/kiosko/:kiosko',KioskoController.getKiosko)
api.delete('/kiosko/:kiosko',KioskoController.deleteKiosko)
api.put('/kiosko/:kiosko',KioskoController.kioskoUpdate)
api.put('/kiosko2/:kiosko',KioskoController.kioskoUpdate2)

module.exports = api