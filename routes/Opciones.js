'use strict'

var express = require('express')
var OpcionesController = require('../controllers/Opciones')
var md_auth = require('../midleware/authenticated')
var api = express.Router()


api.post('/opciones',/*md_auth.ensureAuth, */OpcionesController.saveOpciones)
api.get('/opciones/:pregunta',/* md_auth.ensureAuth,*/OpcionesController.getOpciones)
api.put('/opciones/:opcion',/*md_auth.ensureAuth*/OpcionesController.update)

module.exports = api