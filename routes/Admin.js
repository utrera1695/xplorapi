'use strict'

var express = require('express')
var AdminController = require('../controllers/Admin')
var md_auth = require('../midleware/authenticated')
var api = express.Router()


api.post('/addAdmin',AdminController.guardarAdmin)
api.post('/login',AdminController.loginAdmin)
api.get('/admins',AdminController.listAdmins)
api.delete('/admin/:admin',AdminController.adminDelete)
api.get('/admind/:admin',AdminController.getAdmin)
api.get('/adminemail/:email',AdminController.getAdminEmail)
api.put('/admin/:admin',/*md_auth.ensureAuth,*/ AdminController.adminUpdate)

module.exports = api