'use strict'

var mongoose = require('mongoose')
var app = require('./app')
var port = process.env.PORT || 8083
var eventos = require('events')

//CONEXION A LA BASE DE DATOS
mongoose.connect('mongodb://127.0.0.1:27017/xplor_db',(err,res)=>{
	if(err){
		throw err;
	}else{
		console.log("Conexion a la base de datos establecida")

		app.listen(port,function(){
			console.log("Servidor escuchando por el puerto http://localhost:"+port)
		})
		
	}
})
